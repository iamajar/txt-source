「一良先生，已經是早上了哦，請起來吧。」
「嗯⋯⋯」

隔天早上。
一良感到身體被人搖晃而醒了過來，從大眾旅舍的格子窗框射進來的陽光明亮，大廳裡吵吵鬧鬧的，四周都是正在梳理打扮的旅人。
太陽剛升起沒多久，室內的空氣還頗為清涼。
薇蕾塔和羅茲爾等村人也同樣正在為出門做準備，還蓋著斗篷睡覺的只剩一良和繆菈而已。
有時會在鬧哄哄的聲音中聽見「行李不見了！」的喊叫，應該就是薇蕾塔之前提過的，有小偷在半夜偷東西吧。

「早安。」薇蕾塔看著睜開眼的一良，微笑道。

「早⋯⋯要上街了嗎？」
「是的，再過不久就是開店時間，可以把我們帶來的東西拿去賣了。」

帶來的東西，就是柴火和阿爾卡迪亞虫。
一良不清楚柴火的價錢如何，不過以前薇蕾塔說過阿爾卡迪亞虫是高級食材，而且還有羅茲爾獵到的瑪魯瑪魯的毛皮，所以應該可以賣到相當的金錢吧。
話說回來，一良連這個世界的貨幣單位都不知道，自然也無法預估可以賣多少錢。

「哦，商店這麼早就開門了啊⋯⋯有專門收購柴火的店舖嗎？」
「沒有。不過柴火是所有人都需要的東西，所以隨便找間店就可以賣了。阿爾卡迪亞虫要拿到食品材料行賣，瑪魯瑪魯的毛皮則是拿到布店賣。」
「哦哦，如果是生活必需品，不管哪間店都會收購啊？」

就在一良與薇蕾塔說話時，繆菈也被塔娜叫醒，開始整理儀容。
繆菈完全不會抱怨還想多睡一會兒，一醒來馬上利落地打理自己。這和一良所知的日本小孩天差地別。
見到繆菈懂事成熟的樣子，一良也趕緊把斗篷重新披在自己身上，把滿載柴火的背架背在身上。
離開大眾旅舍後，一行人穿過大清早就人來人往的大道，來到路旁有許多店舖的區塊。
這一區的中央是個寬大的圓形廣場，周圍的建築看起來全是商店。
廣場的直徑約五十公尺，到處停滿了裝載貨物的有篷馬車。
也許是打算就地販賣，有些車主在馬車旁設置了簡單的露天攤位。
這時廣場上已經十分熱鬧了，某些店的門口擠滿了人。
店舖似乎做過某種程度的規劃，食品行和服裝店大致被分在不同的區域。

「那我們先去賣柴火吧⋯⋯一良先生，怎麼了嗎？」

薇蕾塔向一良問道，一良正像昨天一樣半張著嘴呆看廣場。
過去只有在電影裡才能看到的景色，活生生地出現在眼前。
販賣前所未見的食物的商店、販賣真刀真槍的商店⋯⋯第一次親眼目睹到這些景色，讓一良感動不已。

「咦？啊，對不起。我沒想到是這麼大型的商業區，所以不小心看呆了。」

一良身旁的繆菈眼神閃閃發亮地四處張望，看著各式各樣的商店。
看來繆菈也和一良一樣，因為第一次見到的光景而感動不已。

「呵呵，除了這裡之外還有其他商業區哦。更靠近市中心的商業區裡，還有許多販賣高級品的商店呢。」
「唔，這麼說來，這一帶是平民取向的商業區囉？」
「是的，住在愈靠近市中心的居民愈是有錢。」

看來這城市是有錢人住在市中心、其他人住在較偏遠地區的構造。
的確，比起剛到伊斯提利亞時城門一帶的住宅，眼前這一帶的建築物看起來較為豪華。

「好了，我們去賣柴火吧。」
「嗯。」

一行人為了販賣帶來的柴火，走進了最近的一間店舖。
這間商店是石造的兩層樓建築，一樓入口處的門大大打開著。
店裡擺放著大面積的布、類似一良等人穿的草織涼鞋、厚布製的鞋子等等。
眾人走進店裡，看來像是老板的中年男人從店裡面走了出來。

「歡迎光臨，要買什麼嗎？」
「不是的，我們是來賣柴火的，請問您有需要嗎？」

薇蕾塔說著，從一良背上的背架抽出一根柴火給老板看。
老板接下柴火掂了掂重量，滿意地點頭道：

「唔⋯⋯這是很好的木柴呢，他背上的那些賣多少？」
「八十亞爾。」

老板看著一良背上那捆柴火問道，薇蕾塔說出價錢後，老板愣了一下，隨即大笑了起來：

「這位小姑娘，再怎麼說你開的價錢都太高啦。這些的市價是五十亞爾哦。」
「是嗎？那我去其他店裡試試吧。」

薇蕾塔說著，準備走出店外，老板苦笑地微抬雙手，做出投降的動作：

「等等，我出五十五亞爾，小姑娘挺會做生意的嘛。」
「太便宜了。如果您肯出七十亞爾，那我就大放送隨便賣，再少就不行了。」

薇蕾塔停下腳步，皺著眉回頭說出少了十亞爾的價格。
老板也露出困擾的樣子，稍微沉吟了一下後道：

「六十⋯⋯不，六十二亞爾。再高的話就不可能了。如果這個價錢不能接受的話，小姑娘就去別的地方試試吧。」

薇蕾塔將手放在嘴邊，考慮了幾秒後下定決心似地道：

「對不起，我再去別的地方試試。」

她以抱歉的表情說道，老板笑了起來：

「沒關係沒關係，如果其他地方行不通，可以再回來找我。我會用六十二亞爾買的。不習慣講價錢應該很辛苦吧，不過要加油哦。」

薇蕾塔向老板微笑道謝後走出店外。

「薇蕾塔小姐很會做生意呢，那間店的老板答應要用六十二亞爾買下來呢，真是太棒了。」
「真的，我也得好好學學才行。」
「呼──好緊張啊⋯⋯不過，對方稍微讓我賺了一點，看來我不習慣討價還價的事，已經看穿了呢。」

羅茲爾和塔娜慰勞著薇蕾塔，但薇蕾塔苦笑地回道。
一良覺得那是場精彩的討價還價，不過看在職業生意人的眼中，也許兩三下就可以看出她掩飾的緊張吧。
老實說，一良不認為自己可以談到和薇蕾塔一樣的價錢。
可以講到這個價錢，說不定與薇蕾塔的外貌有關。一良是男性，光是先天條件就絶望地比不上薇蕾塔了。

「這些柴火的市價大概多少呢？」
「這些量的話差不多是六十亞爾。如果是冬天就能賣更高一點，但現在這種夏天的話，大致上都是這個價錢。」

一良指著自己背上的柴火發問，羅茲爾回完話後重新背好自己背上的柴火背架：

「那我們也該去賣柴火和毛皮了。繆菈，過來吧。」
「爸爸，我可以和一良大人在一起嗎？」

繆菈不知何時偷塞了一顆糖到嘴裡，臉頰鼓鼓地交互看著一良和羅茲爾。
不過她的手已經緊抓在一良的斗篷上，一副就是想跟的模樣。
說不定是佐久間綜合口味水果糖罐的威力，繆菈在短短幾天裡變得很黏一良。
她可能認為和一良在一起的話，可以又拿到什麼好東西吧？

「啊，我無所謂。我會好好注意別讓她走丟的。」
「可是⋯⋯」

也許是因為對一良心懷尊敬，羅茲爾和塔娜露出抱歉的表情。
但見到以懇求的眼神看著自己的女兒，羅茲爾苦笑地揉了揉她的頭。

「那我們會盡早回來，這段時間裡就麻煩您了。繆菈，要好好聽一良大人的話哦。」
「嗯！」

對於羅茲爾的叮囑，繆菈高興地答應著。
眾人說好事情辦完後在這間服裝店前集合。接著羅茲爾為了變賣帶來的商品，和塔娜一起消失在人群裡了。

「那我們也要去賣東西了。」
「好的，那待會兒見。」

在羅茲爾夫婦之後，同行的其他村民也消失在人群裡，在場的只剩薇蕾塔、一良和繆菈三人。
要賣的東西有阿爾卡迪亞虫和柴火。總之先把柴火賣掉吧，三人走進剛才那間服裝店旁的店裡。
一個小時後。
走出第十間店時，薇蕾塔輕輕嘆了一口氣。
為了不讓繆菈走丟，一良牽著她的手。

「唉，果然沒人願意出高於六十二亞爾的價錢呢⋯⋯」
「是啊⋯⋯雖然有些店願意出到六十一亞爾，可是六十二亞爾以上就很困難了。」

薇蕾塔以在第一間時持同樣的平淡態度討價還價，但果然找不到願意以高於市值六十亞爾的價格買柴火的店舖。
繆菈原本以興奮的神情看著每間商店，可是現在嘴上雖然沒說什麼，臉上已經露出有些疲倦的表情。
假如堅持繼續找肯開高價的店，反而會耽誤到賣阿爾卡迪亞虫和買釘子的時間，最後三人只好回到一開始的那間服裝店。
以距離而言只有數十公尺，他們一下子就走到那間店了。
三人再次走進店裡，老板以「我就說吧」的表情走了出來。

「怎麼樣？其他店頂多出到六十一亞爾對吧？」
「是的⋯⋯對不起，可以請您以六十二亞爾買下這些柴火嗎？」
「嗯。我已經說好了，所以我會買的。」

薇蕾塔語帶歉意地說道，老板笑著從錢包中拿出八枚銅幣交給薇蕾塔。

「十亞爾六枚、一亞爾兩枚⋯⋯金額沒錯，謝謝您。」

一良在薇蕾塔的幫忙下放下背架，把柴火堆在店舖外頭的空地上。

「接下來要去賣阿爾卡迪亞虫了，食品區在那一頭。」

賣掉柴火的三人片刻不停地朝著食品區前進。
食品區的人潮看起來比其他區更多更熱鬧。
每間店前都放著木板或石板，上面寫著店內商品的價格。
一良在前往伊斯提利亞前從薇蕾塔那兒稍微學到了一點這個世界的文字，因此大致上看得懂數字，文字也稍微看得懂一點點。

「呃──夏季地瓜，一個五亞爾⋯⋯咦？一個五亞爾？那麼多柴火也才值六十亞爾，這地瓜真貴啊。是什麼高級地瓜嗎？」
「不，這種地瓜在這個時期我們村子裡也能收成，是很普通的地瓜⋯⋯沒想到會變這麼貴⋯⋯」

看來是因為糧食短缺而導致物價上漲了。
一良朝店裡看去，裝著一個五亞爾地瓜的木箱和其他食物一起陳列在店裡。
那種地瓜長約二十公分，形狀有點像甘薯，大約是拇指和食指圈成的圓圈那麼粗。
就算把賣柴火的所有錢拿來買地瓜，也只能買到十二條夏季地瓜，看來食物類的價錢漲得很凶。
這樣說來，阿爾卡迪亞虫這種高級食材應該可以賣到好價錢，大家滿懷期待地走進店裡，可是店裡老板娘開出的價錢卻是八只十二亞爾。

「對不起啊，現在這種時期，比起填不飽肚子的阿爾卡迪亞虫，地瓜和豆子更重要哦。如果是更市中心的店也許會出更高的價錢，你們要不要去試試呢？」
「是這樣嗎⋯⋯我知道了，我們會去試試的，謝謝您。」

都特地走了兩天的路來伊斯提利亞了，當然希望商品能盡量多賣點錢。
三人離開廣場，朝著更靠近市中心的商業區前進。
皮鞋走在石地板上，發出喀喀的聲響。艾薩克低頭走在納爾森家的走廊上。
他一如既往地穿著皮鎧，但手上拿的不是平時握的劍，而是雙手小心翼翼地捧著色彩斑斕的花束。
艾薩克在走廊上走了一會兒，最後來到通往目的地的門前，他緊張地深呼吸了幾次。
從敞開的窗戶照射進來的陽光很暖和，偶爾拂過臉頰的輕風十分宜人。
艾薩克捧著花束，緊閉雙眼，呼！地調整了一下呼吸。

「奧瑪希歐爾大人，請賜予我勇氣⋯⋯」

他小聲地向自己信仰的戰神祈禱，緩緩睜開雙眼。

「嗚嗚，為什麼會這麼緊張呢？離第一次見面都已經過了四年了啊⋯⋯」

艾薩克按著違背自己意志怦怦亂跳的胸口，再一次深呼吸。
接著他以手指梳了梳剪得短短的金髮，「好！」小聲地為自己加油打氣後，慢慢推開眼前的門。

「真是的！那個垃圾實在有夠煩人的啦！明明把禮物留下來後就可以快點滾了，為什麼我得一直聽他在那邊自吹自擂呢！我怎麼可能和區區的商人結婚，那傢伙為什麼不懂呢！」

整理得乾淨美麗、花草盛開的中庭一角的樹蔭下，一名看起來大約十五歳的少女正趴在木製的桌子上。
少女以煩躁的表情向隨侍在身旁，臉上長著雀斑的年輕侍女抱怨不已。
桌子四周種滿植物，坐在椅子上時只看得到頭，因此趴在桌上時，從外頭是看不見她的。
少女身穿以白色為基底色、鑲著荷葉邊的高級禮服，披散在身後的深褐色長髮帶著美麗的光澤。
五官端正，細長的眸子給人深刻的印象。現在就已經是沉魚落雁的美少女了，長大後肯定是人人稱羨的大美人。
但是少女美麗的臉龐現在正因煩悶而扭曲變形，而且還以穿著高級皮鞋的腳用力地砰砰踹著桌腳。
每當桌子被踹，放在桌邊的有耳銅杯就喀啦地晃動起來。

「可是，莉婕小姐，如果您不喜歡那位先生的話，就別收下他的禮物，明確告訴他您現在沒有結婚的打算，這樣不就好了嗎⋯⋯」

看著正在踹桌子的少女──莉婕，侍女露出有些疲倦的表情說道。莉婕轉頭，狠狠瞪著侍女說：

「我說艾菈啊，如果那麼做的話，我可能就再也收不到禮物了不是嗎？只要用誇張的高興  表情收下那個白痴商人或其他男人帶來的禮物，再隨便敷衍一下結婚的事、笑著聽他們吹噓自己、偶爾稱讚一下他們。這樣一來，那些傢伙就會帶更多禮物送我了不是嗎？而且如果我明確說出沒有和他們結婚的意思，外頭可能會謠傳我喜歡上艾薩克了哦？光是現在，父親大人就已經很希望我和艾薩克結婚了，如果被他逮住機會，可能真的會讓我嫁給艾薩克耶！」

名為艾菈的侍女在心裡嘆氣，「非常抱歉。」不過她還是輕輕低頭道歉。
莉婕和艾菈的這些對話已經重複不知多少次了，艾菈很想把莉婕的行為當成定期發作，什麼都不回應地裝死到底。
可是如果完全不回應，莉婕又會開始生氣鬧別扭。沒辦法，只好還是隨便應付一下這位小姐。
在那之後，莉婕還是不斷嘟噥抱怨不已，不過卻又耳尖地察覺遠遺傳來的、中庭的門被打開的聲音。她立刻挺起趴著的上半身，利落地整理好亂掉的衣服後，拿起桌上的茶杯，優雅地喝起茶。

「您好，莉婕小姐。哦！您正在喝茶嗎？」

剛才因煩躁而扭曲變形的表情已經完全消失了，莉婕側身坐在艾薩克前方四十五度斜對角處，以萬無一失的態勢等著艾薩克。「您好。」她放下手上的茶杯，對從樹籬另一頭走來的艾薩克柔柔笑了起來：

「是的，因為樹木綠得令人心曠神恰，所以我請艾菱幫我備茶，讓我在這裡欣賞美景呢。艾薩克大人也一起來吧？」

莉婕提出邀請，但艾薩克露出打從心底覺得可惜的表情。

「能夠得到莉婕小姐的邀請，讓我倍感光榮。雖然我很高興，但我待會兒就得前往国界，視察建設中的碉堡了⋯⋯」

艾薩克要去碉堡視察的事，莉婕事先就從納爾森那裡聽說了。她心裡說著「對啊，我早就知道了」，但仍假裝驚訝地掩嘴看著艾薩克：

「哎呀，居然要去那麼遠的地方⋯⋯前幾天您才被派去視察遠方的村子不是嗎？現在又要視察碉堡，父親大人也真是會使喚人呢⋯⋯」
「呵呵，您說得沒錯。但納爾森大人願意把視察碉堡這麼重要的工作，交給我這種年輕人做，讓我覺得很榮幸呢。雖然不是件簡單的工作，但只要想到自己能夠成為納爾森大人的力量，就一點也不覺得辛苦了。」

艾薩克說著，露出真心覺得高興的笑容，讓莉婕有些錯愕。

「⋯⋯謝謝您。」

但她隨即綻放花般的笑容向艾薩克道謝。
原本壓抑著緊張的心情，好不容易才有辦法和莉婕正常對話的艾薩克，一見到莉婕那姣好的笑容，立刻變得滿臉通紅，無法冷靜地眼神亂飄，不知該看向何處。

「不！沒什麼！那個，希望莉婕小姐會喜歡！」

似乎是為了掩飾羞澀，他把手上色彩繽紛的花束遞給莉婕。

「哎呀，好美的花束！謝謝您，艾薩克大人。」

莉婕接過花束，宛如對待珍寶似地抱在胸口，對艾薩克露出開心的笑容。
那惹人愛怜的笑靨，讓艾薩克的臉更紅了。

「沒什麼！只要您喜歡，我就感到很榮幸了！那麼我就此告辭！」

艾薩克慌亂地深深行禮後，快步離開中庭。
莉婕目送艾薩克離去，等他的身影完全消失後，把花束塞給艾菈：

「艾菈，其中一半裝飾在我房間，另一半就送你吧⋯⋯等一下，你幹嘛一副看到可怜東西的樣子啊？」

艾菈正以憐憫的眼神看著艾薩克離去的中庭出入口，莉婕把花束塞給她後從椅子上站起，伸了伸懶腰。

「啊──好累啊。今天已經沒有預定見面的客人了吧？」

莉婕無奈地嘆著氣，從懷中拿出以皮繩串起的、三顆顏色幾乎完全相同的藍色寶石。

「好，接下來把其中兩個拿去店裡賣掉，用賣的錢嘩──地大買特買吧。艾菈你有想要的東西就儘管說，我買給你。」
「莉婕小姐，我覺得艾薩克大人是個很不錯的男性，以結婚對象來說，您覺得他有什麼不夠好的地方嗎？」
「插圖」

艾菈交互地看著莉婕與被塞到自己手上的花束問道。莉婕稍微思索了一下，看著艾薩克離去的中庭出入口說：

「哦──是啊，我覺得他是個好人哦。長得夠帥，個性也很好，工作能力也不差，和他結婚的話一定會被他捧在掌心上珍惜吧。不過我沒興趣和他結婚就是了。」
「為什麼呢？」
「因為他的思考模式和父親大人一樣啊。就算我和他結婚、繼承了伊斯提家，我也不能過什麼奢侈的好日子吧？我不是說我打算花錢如流水，可是我也不想和父親大人、母親大人一樣，一輩子過著那種節制簡樸的生活哦。」

莉婕說出令人難以回應的話。艾菈用力忍下想責備她的衝動，沉默不語。
自從艾薩克開始拜訪莉婕，已經過了四年，這些日子裡艾菈與艾薩克碰面的機會也很多。
也許是因為艾菈是莉婕的侍女，艾薩克對艾菈的態度相當尊重有禮，所以艾菈對艾薩克的印象很好。
她一直在心裡為艾薩克與莉婕的事加油，不過從剛才聽到的莉婕想法來判斷，這兩人在一起的機會几近於零。
艾薩克非常努力地討好莉婕，不過莉婕一直都只是釋出表面上的善意，看來就是因為不想和他在一起的緣故。
雖然覺得艾薩克很可怜，不過既然他沒來找自己商量這件事，身為莉婕侍女的艾菈當然也不能主動多說什麼。
艾菈不讓莉婕發現地偷偷嘆了一口氣，再次以憐憫的眼神看著艾薩克離去的方向。
另一頭，一良等人為了販賣阿爾卡迪亞虫，從城市較外緣處來到市中心附近的高級商業區。
愈靠近市中心，四周的建築物就愈宏大氣派，有不少三層樓的建築聳立著。
路上行人的裝扮也如建築一般愈見華麗，而且不時可以看到衛兵的身影。
一良等人到處東張西望，找到食品材料行走了進去。
店裡有些剛才那個商業區的食品行沒有的食物，而且因為物價飄漲，店內商品的價格都相當高。

「歡迎光臨！客人想買什麼？」

一踏入店裡，一名看來是老板的中年男子立刻從店內出現。
他身上衣服的質料看來不差，家境應該頗為富裕吧。

「不是的，我們是來賣阿爾卡迪亞虫的，請問您願意收購嗎？」
「哦！阿爾卡迪亞虫嗎？最近很少有人拿來賣呢。」

老板邊說邊檢視薇蕾塔從小袋子裡拿出來的阿爾卡迪亞虫。

「唔，八只十五亞爾，你覺得如何？」

不愧是販賣高級食材的店。
和剛才那個商業區的收購價十二亞爾相比，多了三亞爾。

「嗯──十五亞爾嗎⋯⋯能再高一點嗎？」
「唔唔⋯⋯真是對不起，我們最多只能出到十五亞爾了。」

都特地來到高級商業區了，以這個價錢在第一間進來的店賣掉阿爾卡迪亞虫有點可惜，薇蕾塔如此拜託著，不過這間店似乎不打算出高於十五亞爾的價錢。
薇蕾塔向老板說她再去別的店試試後，收起阿爾卡迪亞虫走出店外。

「好，我們去下一間店吧。」
「那個，我有個提議，你去賣阿爾卡迪亞虫，我去買釘子如何？我想分頭進行可以省下一些時間。」
「咦⋯⋯一良先生您要一個人去買嗎？」

發現在高級商業區裡能以更高的價格賣掉阿爾卡迪亞虫後，薇蕾塔鬥志高昂地想要朝下一間店前進，但她聽到一良的提議後，以不安的表情看著一良。

「沒問題的啦。每間店外都有寫著價錢的木板或石板，所以我想應該不會被坑到。」
「有我跟著，沒問題的！」

看著四目相望、笑著互相說「對吧──」的一良與繆菈，薇蕾塔噗哧地笑了起來。

「也好。那釘子的事就麻煩您了。錢在⋯⋯」
「啊，錢的話沒有問題。我也從我国家那邊帶了一點東西來賣。」

正當薇蕾塔打算把剛才賣柴火的錢從袋子裡掏出來時，一良提起自己的行囊說道。
薇蕾塔交互看著一良和行囊，雖然不清楚他想賣什麼東西而感到有些不安，但仔細想想，一良應該不會帶什麼便宜貨來賣，所以說道：

「好。如果錢不夠的話請來找我拿。一百根釘子的價錢大約是四十亞爾左右。」
「了解。」

一良說道。他目送薇蕾塔走進其他食品行後，左右張望著廣場，發現了一間雜貨店般的商店，於是和繆菈手牽手朝著那間店前進。

「您好──我有些東西想請您收購──」

一良走進店裡，朝著店後方喊道。
店裡陳列著削成動物形狀的木製小擺飾、打磨得發亮的銅制手鏡等各式各樣的商品。
愈往店裡面走，陳列的商品看起來愈高級，甚至還有綠色土耳其石般的寶石項鏈。
繆菈看到放在店後方的某面銅制手鏡，「哇」的一聲跑了過去。她留意著不碰到鏡子，一臉稀奇地端詳著映在鏡子裡的自己。

「好好好，請問您想賣什麼呢？」

坐在店後方記賬、貌似老板的老婦人因一良的呼喚而停下手上工作，露出善良溫和的笑容，搓著手走到一良面前。
一良從行囊中拿出兩個木製陶笛，上面各有約十個左右的洞，將其中之一拿給老婦人看。
這是一良在日本的個人雜貨店裡，以二九八〇圓買的純手工陶笛，表面塗了亮光漆，反射著美麗的光澤。

「您願意收購這種東西嗎？」

老婦人接過陶笛端詳了一會兒，接著用手撫摸表面，似乎在確認觸感。

「這叫陶笛，您知道這種東西嗎？」

拿著陶笛歪頭苦思的老婦人聽一良這麼一說，笑著看向他：

「哦，對對對，這是陶笛。年紀大了很容易忘東忘西呢，呵呵呵。」

──所以這個世界也有陶笛囉？這樣一來應該就不怕賣不掉了。

陶笛是歷史悠久的樂器。一良是特地先在網絡上查數據，做過功課後再去買的。
付出有了收獲，這讓一良感到很高興。

「我想把這兩個陶笛都賣掉，您願意出多少錢收購呢？」

老婦人撫摸著陶笛，對於一良的問題露出苦思的神色琢磨了一會兒，微皺著眉頭說道：

「最近就算收購了陶笛，賣得也不是很好呢。就算這是用稀罕的木頭製作的，但這種小擺飾還是沒有太多人想買哦。」

老婦人邊說邊不住地摸著陶笛。一良心想，也許她是把陶笛和其他東西搞混了。

「討厭啦，您真是愛說笑，陶笛不是擺飾，是樂器啦。」

一良說著把自己手上的陶笛湊在嘴邊，吹出聲音。
正在端詳銅制手鏡的繆菈聽到音樂聲，跑回一良身邊，眼神閃閃發亮地仰頭看著一良：

「哇啊，好美的聲音⋯⋯真好聽。」

聽到陶笛聲音的老婦人驚訝地瞪大眼睛呆住了，但是一良並沒有發現。他停下吹奏，笑著對繆菈道：

「來，吹吹看。」

繆菈接過陶笛後馬上將它湊在嘴邊，高興地嗚嗚吹了起來。
一良微笑地看著繆菈，接著把視線轉回老婦人身上。老婦人趕緊收起驚訝的表情，像剛才那樣一臉慈善地笑著搓手：

「哦，是啊是啊，陶笛是樂器呢。不過，就像剛才我說的，不是很多人買哦⋯⋯這位客人您想賣多少錢呢？」
「咦？⋯⋯唔，這個嘛⋯⋯」

一良雙手交叉在胸前盤算著。
自己花了那麼大力氣從村裡背來的柴火，總共也只賣了六十二亞爾，木製的陶笛應該不可能更貴吧？
而且照老婦人的說法，雖然這個世界上也存在著陶笛，但似乎不是什麼昂貴的東西。
總之，如果可以賣到這個價錢就好了。一良抱著這種希望試著說出心中的高價：

「一個二十亞爾如何呢？」
「噗？」

聽到一良提出的價錢，老婦人一陣咳嗆，似乎覺得那是很不合理的價格。
見到老婦人的反應，一良心想自己的行為該不會是漫天喊價吧？他趕緊訂正道：

「啊，假的假的，我是開玩笑的！兩個二十亞爾如何呢？」
「兩、兩個二十亞爾！？好我知道了！我現在就去拿錢，請等我一下！」

一良修正價格後，老婦人急急忙忙地走到店後面，很快地拿了兩枚銅幣回來。

「嗯，是二十亞爾沒錯⋯⋯唔，可是這樣不夠買釘子呢。」

收下兩枚十亞爾的銅幣後，繆菈把自己吹過的陶笛交給老婦人，一良回想自己袋子裡的東西思考著。
站在一良正對面，收下兩個陶笛的老婦人正喜不自勝地不斷撫摸著它們。
一良看著這樣的老婦人心想：

──她該不會是特別喜歡陶笛的人吧？

做出錯誤解讀的一良再次張望店裡。
他發現靠裡頭的位置上放了各種色彩的石頭。
雖然透明度不高、加工技術似乎也很粗糙，不過其中也有一些像藍寶石般的小石頭。
見到那些石頭，一良翻著行囊，拿出一顆直徑約兩公分的紅水晶珠子。
這顆紅水晶是一良在常去的那間大型五金行附近的土產店買的。賣能量石的那區有一大堆石頭，一良覺得也許能拿到異世界賣，所以買了幾顆。
以機械切削的紅水晶（似乎也稱為粉晶）呈透明的粉紅色，看起來很美麗，但也許因為那是大量製造的工業產品，所以一顆只要二五〇圓，相當便宜。

「那個，請問您可以連這個一起收購嗎？」

一良說著，把紅水晶交給老婦人。老婦人將原本不斷撫摸的陶笛放在附近櫃子上，不知為何雙手顫抖地接過了紅水晶。

「嗯、嗯、嗯嗯、我買！我買！」

老婦人死命盯著紅水晶看，露出奸詐的笑容。
不過下一秒她就恢復成原本慈善的模樣，滿臉笑容地看著一良，所以一良以為是自己眼花了。

「是嗎？那真是太好了，請問您願意以多少錢收購呢？」
「這個嘛⋯⋯像這樣的一顆，兩百亞爾如何？」
「兩百亞爾！？你說這顆兩百亞爾！？」

兩百艾爾。對於這出乎意料的金額，一良反射性地一邊大叫，一邊把紅水晶從老婦人的手上拿回來。
接著他和剛才的老婦人一樣盯著紅水晶猛看，老婦人不知為何很慌張地開口：

「啊！不對！是我弄錯了！是兩千亞爾！我會用兩千亞爾買下它！！」
「⋯⋯啊？兩千亞爾？」
「對！我會用兩千亞爾買下它⋯⋯的？」

老婦人突然高聲喊叫，把紅水晶搶了回去。一部分的原因是見到至今為止從沒見過的美麗寶石，讓她處於極度興奮的狀態下；另外就是，她誤以為是價錢開太低，讓一良生氣了。
而且更糟的是，因為再怎麼隨便賣都肯定能賣到三千亞爾以上的價格，所以不小心說溜嘴，開出了兩千的收購價。
但在看到一良呆愣地張大嘴的傻樣後，老婦人終於發現自己犯下大錯，覺得全身的血液一下子全部倒流。

「一良大人，這個牌子上面寫說，這邊的石頭一顆一千二百亞爾呢，好貴啊──」

老婦人連忙想訂正自己的發言，不過繆菈打斷她話似地，仰頭看著店後方放寶石的架子說道，就算想訂正也來不及了。
直到此時，一良才終於發現自己被騙了。
回想剛剛的交易，看來陶笛應該也被相當程度地賤賣了吧？
不過既然已經成交了也莫可奈何，只能說幸好紅水晶還沒被賤價收買。
一良大大嘆了口氣，反省自己的單純天真。陶笛的部分就當成社會學習的學費，放棄討回了。

「哦──那就兩千⋯⋯不，可以請您用兩千五百亞爾買下嗎？不然我拿到別間店賣也是可以的。」

假如老婦人沒判斷錯誤就能发筆橫財了，但就算是用兩千五百亞爾收購，也還是有相當的利潤。
老婦人咒罵著自己的粗心大意，以幾乎快聽不見的聲音小聲說著：「⋯⋯多謝了。」

「哇啊，這就是一百亞爾的銀幣嗎？我是第一次看到呢。」

由於繆菈吵著要看一百亞爾的銀幣長什麼樣子，所以一良從老婦人給他的二十五枚一百亞爾銀幣中，拿了一枚給她。
一百亞爾銀幣比一亞爾銅幣打上兩倍，和十亞爾銅幣差不多大，但材質是銀，而且上面的花紋比十亞爾銅幣細致許多。
繆菈高興地接過銀幣，不知為何或是輕咬或是咚咚地敲著銀幣玩了越來。
一良側眼看著繆菈的動作，向正以認真表情從各種角度檢視紅水晶的老婦人說道：

「夫人，我有個提議⋯⋯」
「哦，我不會告訴任何人這東西是從哪裡收購的，也不會追問你這東西是從哪裡拿到的。下次我會用正當價格收賻你的東西，有多少要賣的就儘管拿來吧。」
「⋯⋯您真是明察秋毫呢。」

老婦人端詳著紅水晶，以早就料到一良想說什麼的口氣平淡地回應道。
連提議的內容都還沒說出來就被如此響應，這讓一良有些驚訝。不過老婦人的發言和一良想說的事差不多，所以可以很快切入正題，也算是好事。

「你從袋子裡拿出這顆寶石時，不是有挑選了一下嗎？所以還有其他東西可以賣對吧？不管是剛才的陶笛還是這顆寶石，像你們這種鄉下人身上居然有這種高價品，我當然不會認為你們是用什麼正當手法拿到這些東西的。」
「可是，我也不是用什麼不好的手段拿到的啊⋯⋯」

老婦人仿彿認定這些東西是從哪裡偷來的，這讓一良很困擾。

「嗯，你是怎麼拿到這些東西的我完全不在意，只要可以讓我賺到錢，其他都無所謂啦。」

無視一良的反駁，老婦人將視線從紅水晶上移開，邪邪一笑。
感覺自己似乎被當成匪徒，一良有些不高興，不過她不打算追根究柢這點倒是好事。
如果帶來的紅水晶和陶笛都能賣到極高的價錢，那麼四處變賣反而會引人倒目，還不如集中在某間店低調地處理。
由於差點被眼前這名老婦人坑殺（雖然陶笛被騙走了），一良終於找回了自己的危機意識。回顧這段時間裡自己的天真無邪，也許反倒該慶幸剛才被詐騙的事。
那真是太好了。一良有口無心地回應著老婦人的話。老婦人把紅水晶放在附近的商品架上後，重新看向一良：

「你要把袋子裡的其他東西也拿出來賣嗎？我不會再騙你了，所以放心拿出來吧。」

老婦人再次以慈善的笑容搓手說道。一良嘆了口氣：

「很遺憾，今天要賣的東西就這些而已。就算我有其他東西可以賣，如果乖乖聽話把東西拿出來，就真的是大傻瓜了。」
「哦，至少懂這部分，不是完全的笨蛋。太好了。」

老婦人因一良的回答而笑出聲。「可是啊⋯⋯」她接著說道：

「你還是太沒有防備了。雖然我不知道你是偶然進來我這問店還是怎樣，不過來我這裡算你運氣好哦。像你這種鄉下人，如果是在其他店裡拿出這種高級寶石，就算被當成盜賊叫衛兵來捉拿你也不奇怪。而且要是在官員親戚開的店裡拿出這種東西，說不定還沒賣掉你就完了。這城市多的是死腦筋的官員呢。」
「⋯⋯謝謝您的忠告。」

被欺騙自己的對象教訓令人不快，但老婦人說的正是一良擔心的事。
一良沮喪地垂著肩，老婦人嘆了一口氣後，朝店後方說道：

「小妹妹，那個櫃子沒有鑰匙是打不開的哦。放心吧，就算不和櫃子裡的銀幣比對，剛剛給你們的錢也是真的，不用懷疑。」

一良聽老婦人這麼說，吃驚地朝著店裡頭看去，繆菈不知何時已經跑到店後面，抓著剛才老婦人拿出銀幣的抽屜把手想打開它。
聽見老婦人的話，繆菈似乎有點驚訝地雙肩一顫，但又隨即以猜忌的眼神看著老婦人，似乎仍在懷疑銀幣的真假。
原本高高興興地逛著店裡的繆菈，在一瞬間轉變成以帶著明顯敵意的眼神瞪著老婦人，這樣的變化讓一良感到有些驚訝。
老婦人無奈地從懷中拿出鑰匙，走到繆菈那裡打開抽屜。

「你就確認到高興為止吧，我向蓋耶爾希歐爾大人發誓，裡面絶對沒有偽幣。」

老婦人說完，繆菈馬上以認真的表情對比起手上的一百亞爾銀幣與櫃子裡的銀幣。

「真是的，這小姑娘還比較機靈呢。你也振作點啊！」

老婦人看著比對銀幣的繆菈，拍著一良的背說道。
一良無法反駁，「我會銘記在心的⋯⋯」只好垂頭小聲地如此說道。

「我討厭那個老婆婆。居然敢欺騙一良大人，真是難以置信！」

一走出老婦人的店，繆菈就恨恨地啐道。
等繆菈比對到滿意之後，一良想向老婦人購買店裡的釘子。也許是因為繆菈一直以帶刺的眼神瞪著老婦人，老婦也因此覺得不舒服吧？「以後也請你多多關照。」她如此說著，以四十亞爾的價格將兩百根釘子賣給一良。
接著她又從店裡拿出裝了豆制點心的小袋子，送給繆菈以討她歡心，不過繆菈直到最後還是沒給老婦人好臉色看。
雖然不給好臉色，不過還是收下了點心。

「是啊，騙人是壊事。不過我太蠢了也不對。」
「一良大人才沒有錯呢。錯的是欺騙一良大人的那個老太婆！」

繆菈似乎無法諒解一良被騙的事，眼中含淚地仰望一良。
一良趕緊安撫繆菈，這 他看見羅茲爾與塔娜走進視野的邊緣。
原本背在背上的柴火和毛皮消失了，取而代之的是個布袋，看來已經買賣完畢了。

「哦！繆菈，你爸爸媽媽在那邊哦，看來事情已經辦完了呢。」

一良說完，繆菈原本悶悶不樂的表情轉為明亮。

「啊！真的耶！爸爸、媽媽！」

繆菈一邊大叫一邊朝著父母跑去。
一良在心裡感謝羅茲爾夫婦出現的時機正好，為了追上跑走的繆菈而邁開腳步。
就在這時，一良的鼻子撞上了從斜前方突然跑出來的人的側頭部，一良因此跌坐在地上。

「好、好痛⋯⋯」
「對、對不起！我沒注意到左右⋯⋯好痛。」
「艾菈，你在做什麼啊！」

一良手按鼻子看向說話的人，一名穿著侍女服的女性正摸著頭呻吟，旁邊有名穿著高級白色禮服、長相極美的少女快步跑了過來。

「真是對不起，我的侍從撞到您了！她太注意奔跑中的孩子所以和您相撞⋯⋯您要不要緊呢？」
「咦？呃，不，沒什麼大不了的，請別在意。」

少女似乎不在意弄髒禮服，她直接蹲跪在地上，將手放在一良肩膀，以擔心的眼神看著一良。這讓一良覺得很困惑。
光看一眼就知道，少女的服裝比四周的人高級了好幾個層次，而且還有侍從陪伴。
身分如此高貴的人物，居然會如此對待自己這種鄉下人，光是這樣就很讓人驚訝了，更重要的是少女長得十分美麗，讓一良不禁面紅耳赤起來。

「您行李的東西也掉出來了呢。艾菈，你也快來幫忙撿吧。」
「是、是的。真是抱歉。」

一良因少女的話而向地面看去，接著臉色發青。
他跌到地上時，行囊的繩子也鬆開了，裡面的東西掉了一些出來。
不過仔細一看，掉出來的東西是為了預防出事而放在上層的青銅鍋子跟木杯之類，從日本帶來的罐頭和油燈大概因為收在底部，所以沒滾出來。
一良趕緊收拾掉出來的行李，由於還有兩個人幫忙，東西一下子就收好了。

「咦，我腳底下好像有什麼⋯⋯」

放眼望去，能看到的東西都已經迅速收進袋裡，一良鬆了口氣。就在此時，少女突然發現自己鞋子下有東西，撿了起來。
是打火機。

「對、對不起！那也是我的東西！」
「是嗎？這就還給您⋯⋯咦，這是什麼⋯⋯！？」

也許是對打火機上凹陷的點火鈕感到好奇，少女將其按了下去，火焰隨即轟然冒出。
由於是按壓式打火機，再加上之前一良為了點燃柴火而把火力調到最大，因此冒出來的火柱頗為驚人。
少女吃驚地縮手，打火機掉到地上。
穿著侍女服的女性──艾菈，也因為看到從打火機冒出的火而張大眼睛。

「莉婕小姐！」

兩名平民打扮的年輕男人從數名看熱鬧的鄉民中竄出，朝著因驚嚇而收手的少女──莉婕跑來。
雖然身處在大街上，但男人們的腰間配著劍。

「莉婕小姐，您怎麼了！？」
「咦！？剛剛有火⋯⋯」
「你這傢伙在幹什麼！不准動！」

正當一良慌張地想撿起名為莉婕的少女掉下的打火機時，其中一名男子扭住一良的手臂抓住他。
這兩人應該是暗中保護莉婕的護衛吧。

「好痛！我、我什麼都沒做啊！真的啊！」

她果然是身分高貴的人，一良在心裡咒罵著自己運氣不好。他雖然想找藉口搪塞，不過只要打火機被他們撿去，一切就完了。
一良祈禱著自己不會被不由分說地處決，但就算是被帶回去審問，也一定會被嚴加追問行李裡的東西吧。
再會了，溫馨的異世界農村生活啊。正當一良悲觀地想著時，羅茲爾等人從莉婕身後跑了過來。

「請問我的同伴惹了什麼事嗎？」

羅茲爾向扭住一良手臂的男人問道，男人以凌厲的眼神瞪著他：

「什麼！你是這傢伙的同伙？他想要加害納爾森大人的千金──莉婕小姐。我看你們也得好好審問審問才行，跟我到衛戍室去吧！」
「慢著！這位先生沒有冒犯我！」
「是、是啊！是我不小心撞到人家的，這位先生什麼都沒做！」

納爾森的千金。這話讓羅茲爾全身僵直，不過他又馬上深深低頭，不斷賠罪。
這下子大事不妙了。一良也在胳膊被扭住的情況下連連道歉。
可是護衛們還是無視他們的道歉，從腰間拿出繩索，不由分說地打算綁住兩人。莉婕和艾菈連忙介入其中。
一直以來莉婕都多多少少感覺到父親的私兵尾隨自己，但這是第一次直接出面干涉她的事。

「放開他。」
「但是⋯⋯」
「別讓我說第二遍。」
「⋯⋯是！」

莉婕不容忤逆地悍然道，男人不情願地放開擒住一良的手。
見一良被釋放，羅茲爾朝莉婕深深行了一禮。
一良也學著羅茲爾，向莉婕深深低頭道謝。

「謝謝莉婕小姐大人不記小人過，我們就此告辭。」
「啊，請等一下，剛才掉下去的那個會起火的⋯⋯咦、怎麼不見了？」

莉婕叫住準備開溜的一良一行人，找起剛才掉在腳邊的打火機，不過怎麼找都沒看到。
羅茲爾見莉婕的樣子，再次向她行禮告辭，推著一良的背朝廣場入口走去。

「一良大人。」

走到離莉婕等人有段距離後，繆菈伸手要求和一良牽手，一良握住她的手後，發現打火機正捏在她手掌中。
就在莉婕和一良他們吵吵鬧鬧時，繆菈趁機偷偷把掉在地上的打火機撿回來了。

「謝謝，你幫了我大忙哦。」

一良以眼角餘光確認繆菈手裡的東西後，將打火機收入袋子裡，溫柔地撫著她的頭。

「嗯！」

被摸頭的繆菈高高興興地叫應道，再次和一良手牽手走了起來，表情十分滿足。

「真是好險啊。要不是莉婕小姐幫我們解圍，下場不知道會是怎樣呢。」
「莉婕小姐⋯⋯剛才抓著我的男人說她是納爾森大人的女兒？」

一良向鬆了口氣的羅茲爾問道，「是的。」羅茲爾點頭。

「常聽人說莉婕小姐雖然年輕・可是非常慈悲，就連對待我們這些平民都很親切和善，今天總算是見識到了。不愧是納爾森大人的千金啊。」
「我也聽說附近有權有勢的人都在追求莉婕小姐呢。身為伊斯提家的千金，人長得美、風評又好，真是無可挑剔啊。難怪有那麼多人想追求她。而且莉婕小姐雖然年輕，但聽說她的劍術和槍術也很高明哦。」

羅茲爾與塔娜邊走邊讚美著莉婕。「原來如此。」一良點頭道。

「唔，雖然俗話說天不予人二物，但世界上還真的有這麼完美的人呢。」
「天不予人二物？」

羅茲爾與塔娜不懂一良說的諺語。

「意思是，上天不會把許多天賦和才能賜給同一個人，是我的国家的諺語。」

就在一良與羅茲爾等人聊著這話題時，他們發現了站在廣場入口的薇蕾塔。
薇蕾塔也發現了一良等人，輕輕揮著手朝他們走來。

「您和羅茲爾大哥碰頭啦？釘子已經買好了嗎？」
「嗯，我買了兩百根釘子，這樣一來應該就做得出水車了。」

聽到一良這麼說，薇蕾塔鬆了一口氣微笑起來。

「一良大人帶來的寶石賣了二千五百亞爾哦！」
「咦！？」
「等一下，這種事不能大聲說啦！」

一良連忙按住繆菈的嘴，對眼睛瞪得像銅鈴大的薇蕾塔等人解釋起在雜貨店發生的事。
當天晚上。
與明亮的燭光相襯的豪華房間內，莉婕正與雙親共進晚餐。
餐廳大約六坪，是納爾森為了讓家人團圓吃飯而特別裝潢過的房間。
地板上舖著以動物毛皮制成的地毯，牆壁上設置了一個壁爐，但現在沒有燃火使用。
房間唯一的窗戶是敞開的，可以看見被燭火照亮的中庭。
時值夏季，偶爾從窗口吹入的晚風相當宜人，虫嗚雖然無法稱為音樂，但也釀造出了相當好的氣氛。
三個人使用的長型餐桌不大，再放兩份餐具就沒有空間了。
順便提一下今晚的菜色。每人各一份的是鹽烤河魚、豆子蔬菜湯、麵包以及作為餐後點心的切片水果。
除此之外還有一大盤的蔬菜炒肉放在餐桌的中央。

「莉婕，每個月都有那麼多人向你求婚，你找到滿意的對象了嗎？」

以叉子將切下來的魚肉送入口中的父親問道。莉婕在心裡嘆氣，不過表面上還是裝出笑容：

「不⋯⋯每位駕臨的大人都很優秀，但是我還沒考慮結婚的事⋯⋯」

是這樣嗎？聽莉婕這麼說，納爾森點頭笑了起來：

「也不必急，早晚會出現讓你心動的男性的。只要是你中意的對象，不管是貴族或平民我都無所謂哦。不過成為伊斯提家的成員之後，我會徹底鍛鍊女婿就是了。」

──你明明就希望我嫁給艾薩克，所以開始鍛鍊他了不是嗎？

莉婕在心裡吐嘈，不過還是裝出笑臉道：「謝謝。」

對莉婕來說，「對象是平民出身也無所謂」的這種貼心根本沒必要，因為她完全不考慮和貴族之外的人結婚。
雖然不需像其他貴族的女兒一樣，被迫嫁給雙親擅自決定的對象，這點讓莉婕很感謝；不過可以的話，莉婕還是想和有錢的貴族結婚。
從小到大，莉婕的人生中沒有什麼不如意的地方，但是和其他貴族、王家相比之下一點也不華麗的生活，總是讓她有種自卑感。
雖然私人服裝、招待來自其他領地客人的房間相對豪華，可是除此之外的部分，全都盡量節省開支。
由於阿爾卡迪亞與巴貝爾之間的戰爭，使得最近十幾年來伊斯提家的財政急遽惡化，也因此更加徹底地撙節用度，想過優雅的生活可說是夢中之夢。
四年前，莉婕跟著參加領主會議的納爾森前往王都。那時她見到王族與圍繞在王族周圍的貴族們優雅的生活，讓她受到極大的震撼。
住在王都的期間，雖然莉婕當時才十歳左右，但由於容貌已經相當出眾，許多貴族都開始追求莉婕。
有些確定莉婕將來一定會成長為美女的人，甚至送了小山般的美麗衣裳與寶石等豪華禮物給她，以求能在莉婕心中留下深刻的印象。
之後，納爾森不知為何不再帶莉婕去王都了，但是當時的事仍然深深印在莉婕心中，她也開始對自己身為貴族卻無法過著那種優雅生活感到不滿。

「不過，結婚對象要慎選才行呢。雖然身分與家族勢力也很重要，但是我覺得和可靠又溫柔的人結婚，絶對是更好的選擇哦。其他不足的部分，只要在婚後好好訓練老公就行了。」

母親微笑地說道。莉婕也笑著回答「是」，但內心有種複雜的感覺。
母親的名字是吉珂妮亞。
年紀才二十六歳，是十年前阿爾卡迪亞與巴貝爾開戰前夕，父親納爾森再婚的、行伍出身的平民女性。
及肩程度的銀髮、沒有贅肉的勻稱身材，五官的最大特色是一雙穩重的下垂眼。
雖然不像莉婕美貌過人，但給人一種容易親近的感覺。
莉婕的生母在她三歳時就病死了，在那之後到五歳為止，莉婕都沒有母親。
因此，當吉珂妮亞剛出現時，莉婕因為怕生而無法坦然面對她。不過吉珂妮亞積極地與莉婕親近，因此沒多久兩人就處得很融洽了。
但是，當莉婕第一次前往王都的一年後，她開始對吉珂妮亞所做的某件事感到不愉快。
而且那還是從侍女艾菈那兒聽來的。就是與莉婕見面的那些客人，都已經事先被吉珂妮亞挑選過了。
一直有許多貴族與富豪聽聞莉婕的美貌，前來請求與她見面，不過只有吉珂妮亞同意的人才見得到莉婕。
連從王都遠道而來的大貴族，甚至是治理隔壁領地的大貴族戴亞斯・古雷葛倫，都因為吉珂妮亞的自作主張而不讓他們與莉婕見面。
當然吉珂妮亞不是直接拒絶，而是由納爾森出面編造各種理由來婉拒。但對莉婕來說，這麼做根本是多管閑事。
原本就是大貴族的伊斯提家，在阿爾卡迪亞與巴貝爾間的戰爭爆發後，發言權也變大許多。因此被拒絶的貴族們無法強行要求見面，只好帶著不滿，摸摸鼻子回去了。
莉婕不清楚吉珂妮亞是以什麼基準來拒絶那些人，但是她對吉珂妮亞擅自作主的這一點無法接受。
可是既然連納爾森都幫著吉珂妮亞，莉婕也只好忍下來了。
見到工作繁重、片刻不得休息的雙親特地空出時間，就為了和女兒共進晚餐，莉婕當然無法叛逆。
因此她決定先裝成順從的模樣。並且為了在有機會前往王都時能找到好對象，所以除了通過吉珂妮亞的挑選前來見她的人之外，莉婕也努力提高自己在伊斯提利亞居民心中的好感度，以提升自己在外頭的風評。

「對了，今天我命令艾薩克去視察建在国界邊緣的碉堡⋯⋯」

不知是否是明白莉婕的心情，納爾森開始提起自己中意的女婿候補──艾薩克的事。

「煩死人了啦！」

結束與雙親的晚餐時間，回到臥房的莉婕趴在有柔軟頂篷的床上，將臉埋在枕頭裡。

「到底是要我怎樣啊──！！」

接著，她用盡全身力氣怒吼。
最近只要到了吃飯時間，雙親就一定會提起艾薩克的事，但這對莉婕來說一點也不重要。
雖然她明白雙親很中意艾薩克，不過既然嘴上說要讓莉婕自己挑選對象，就別做出幫某個特定人物抬高身價的舉動啊。

「我管艾薩克去死啊！別再說他的事了啦！」

如果艾薩克聽到這些話，可能會上吊自殺。正當莉婕一邊在床上亂動，一邊對著枕頭大吼時，入口處傳來敲門的聲音。
莉婕立刻坐起，以放在床頭櫃上的梳子理了理頭髮，把亂掉的衣服拉好後，輕咳了一下道：

「是哪位？」
「我是艾菈！打擾了！」

艾菈還沒得到莉婕的許可就匆匆忙忙地闖進房裡。

「莉、莉婕小姐！要要要、要怎麼辦！？」
「等一下！你冷靜點啊！怎麼了？」

莉婕以放在圓桌上的水瓶倒了杯水給一臉慌張的艾菈。

「真、真是抱歉。」

艾菈一邊喘氣一邊接過杯子，喝了一口水後道：

「我在房裡換下圍裙時發現的，我圍裙的口袋裡有這、這、這個⋯⋯」

她一面說著，一面從口袋裡拿出鑲著心形銀邊、顏色偏藍的半透明寶石──人工蛋白石的項鏈墜飾。
被鑲在銀邊裡的人工蛋白石反射著房間的燭光，發出彩虹般的光輝。

「⋯⋯！？」

兒到那墜飾的瞬間，莉婕的思考停止了。
她曾見過許多寶石和飾品，可是不曾見過這麼美的東西。

「該、該怎麼辦？應該是我白天撞到的那位先生掉下來的⋯⋯莉婕小姐？」

莉婕無視艾菈的話，被墜飾奪去心神似地僵在原地，直到艾菈拍著她的肩膀，好不容易才回過神。