# 含有日文的章節段落

[TOC]

## 00000_文/00000_第一卷/00080_後記

- 雖說是處於私事，我很久以前就想挑戰不死者的故事，寫起來十分暢快。如果你能和メロントマリ老師繪製的氣氛恰到好處的插圖一起享受，我會很高興的！
- 負責本作品插圖的メロントマリ老師。最初看到插圖時的衝擊至今還記憶猶新。恩德、森麗，還有露和支配者我都很喜歡。我會為了能寫出與插圖相符的作品而努力，今後也請多多關照。在出版之際竭盡全力的編輯和田先生，以及給予我幫助的Fami通文庫編輯部的各位。我這拙作變成精美的書籍都是多虧了大家。我今後也會全力執筆，請多關照。以及最重要的，我要借此機會向長期支持我們的讀者們，以及從書籍版開始的讀者們表示衷心的感謝。謝謝大家了！
