散步後的第二天，蕾蒂雪爾在學園的學生食堂裡和朋友們一起吃午飯。

「那個，我就先告辭了。」

靜靜地從椅子上站起來，維羅尼卡向蕾蒂雪爾他們點頭示意。上周沒有上學的她，現在好像完全恢復了健康。

「嗯，去幫忙嗎，加油喲。」
「是！」

維羅尼卡握著她那小小的拳頭。她打算在這之後回教室去幫教師。雖然蕾蒂雪爾不知道，不過，好像維羅尼卡是真的工作仔細，放學後經常這樣很好地幫助著老師的工作。

「還有一周就是暑假了。大家打算怎麼度過呢？」

餐桌上只剩下蕾蒂希爾和吉克和對面的米蘭達蕾特和希爾梅斯四人。米蘭達蕾特攤開雙手問到。

「我大概要到很久沒去的軍官學校去露個臉。」
「哎呀，希爾梅斯要去了軍官學校啊」

在蕾蒂雪爾生活的時代沒有軍官學校，讀了書後才知道在這個国家有專門培育率領軍隊的領導人的機構。

「是的。嘛，雖然我去的是預科而不是本科就是了。」

在這個国家貴族就讀的學校，除了盧克雷齊亞學園以外還有軍官學校。軍官學校有本科和預科兩個部分，預科從１３歳到１５歳，本科從１６歳到１８歳都可以上課。１６歳以上的貴族必須去學園或者軍官學校本科的其中一個上學。普拉提納王国的貴族子女原則上有去學園的義務，但那隻限於女子和要繼承家業的男子。因此，次子以下的令子們不去學園而去就讀軍官學校本科的情況很多。

「原來如此。所以希爾梅斯很擅長劍術呢」
「非常感謝您的誇獎！我家是騎士家庭，所以弟弟們從預科可以直接進入本科。只是因為我是嫡子，所以要在這邊⋯⋯」

我也想去本科啊！這樣叫著的的希爾梅斯，果然還是喜歡劍術啊，蕾蒂雪爾坦率地微笑著想到。

「說起來，我昨天去了城下街喲」
「誒！？這樣啊！你去哪兒逛了？」

對著像是才想起了似的喃喃自語的蕾蒂雪爾，米蘭達蕾特驚訝地大聲叫著，用羨慕的目光看著這邊。

「和管家一起去希路美爾商會總店買東西。」
「我聽說過！是王都規模最大的商店吧？我還沒去過呢⋯⋯」

作為烏爾德男爵家三女兒的米蘭達蕾特，好像還沒有逛過街。

「那一帶好像是⋯⋯有一個很簡陋的書店啊。雖然不大，但是有很多可以挖掘的書。」

鄰座的吉克說出了一個十分讓人感興趣的情報，蕾蒂雪爾的眼睛開始閃閃發光了。

「有那種店嗎？」
「嗯。因為希路美爾商會總店很大，周圍的店為了展現自己的特點而努力表現著。」
「這樣啊。」

蕾蒂雪爾沉思著。如果是那樣的話，昨天應該再看看別的店，也許做了件可惜的事。

「啊，還有總店後面有一家烤串小攤。有次偶然去吃了一會，很好吃哦。」
「燒烤⋯」
「還有⋯⋯是啊⋯⋯我還推薦羅莎先生經營的咖啡店的鮮榨果汁。」
「鮮榨果汁⋯」

之後討論的，就完全是尼爾瓦恩隱藏的有意思的地點了。在蕾蒂雪爾生活的時代不可能有的，而且從未聽說過的食物和道具的話題一個個提了出來，蕾蒂雪爾聽得入迷的都忘了時間。

「吉克，你知道的真詳細啊。」
「這也是因為我是在大街上長大的」

聽了蕾蒂雪爾佩服的話，吉克稍微得意地笑了。一不留神就忘了，吉克因為沒有中間名，所以不是貴族階級。他確實應該對街道的情況很了解。

「那下次大家一起去？我帶路。」
「好啊好啊，一定會很高興的。」

對於吉克的話，米蘭達蕾特看起來很高興地拍手。旁邊的希爾梅斯也浮現出了津津有味的表情。

「如果可以的話我也想和大家一起去看看呢。」

蕾蒂雪爾也馬上點頭。自己還一次沒出去過街外，想到能得到新的知識，從今往後多得是樂趣。

「要邀請威羅尼卡嗎？」
「啊，那我問問看。正好是同班。」

快樂的時光一轉眼就過去了。很多學生在食堂裡，回過神來發現很多學生都到了出入口準備回去。

「馬上就要上課了啊。」
「時間過得真快啊。大家上課請加油哦。」

對米蘭達蕾特的自言自語，蕾蒂希爾輕輕地揮手鼓勵道。

「是的，我會加油的，走吧，利夫。」
「喂，等等，露露。」

用力地點頭後，米蘭達蕾特幹勁十足地跑開了。希爾梅斯慌忙地追趕著他那精神的未婚妻。二人出去之後，學生的數量持續減少，最終只剩下作為偷懶慣犯的蕾蒂雪爾和吉克。

「多蘿瑟你打算怎麼辦？」
「像往常一樣呆在研究室裡。吉克下午打算怎麼過？」
「我想在大圖書室讀書。」
「原來如此，那我們順路走一會兒？」
「好啊，務必。」

蕾蒂雪爾和吉克一起前往別館，在大圖書室前分別了。

「那麼，今天能改造到哪種程度呢⋯？」

考慮著今天打算改造的術式，蕾蒂雪爾高高興興地走向自己的家（研究室）

「學院長，我今天就到這裡了。」
「哦，小心點。」
「謝謝你的關心。」

時間到了傍晚，蕾蒂雪爾像往常一樣把下午的時間花在研究上，然後結束今天的預定，離開了校長室。
下樓梯走到了一樓的走廊上，視線前方大圖書室的門緩緩打開了，有人在走廊裡現身。

「吉克？」

從大圖書室出來的是吉克。對面也注意到蕾蒂雪爾而愣住了。

「真是奇遇啊，多蘿瑟露小姐。難道說，你現在開始要去魔法訓練場嗎？」
「嗯，和以往一樣，你也要一起去？」
「好的。」

因為目的地是一樣的，所以兩個人一起走。目的地當然是魔法同好會的活動場所──魔法訓練場。

「對了，吉克，你手裡拿著的書是什麼？」
「啊，是這個嗎？這是關於這個国家的紡織品的書。」
「嘿⋯⋯吉克對織物很感興趣呢。」
「嗯，我個人覺得很有意思⋯⋯」

兩人一邊漫無邊際的閑聊著，一邊走在與本館連接分館的走廊上。

「啊！」

聽到了從左邊傳來的聲音，蕾蒂雪爾停了下來。

「你好！」

打招呼的是戴著氈帽，背著大包的小販。留著懶散的鬍子，腰上插著短劍，打扮得很輕便。咦？這個人⋯⋯⋯蕾蒂雪爾不由得盯著小販的臉。

「多蘿瑟露小姐，你的熟人？」

吉克交替地看著蕾蒂雪爾和小販。蕾蒂雪爾陷入了沉思。好像在哪裡見過似的，又好像沒有⋯⋯⋯蕾蒂雪爾視線向下看向小販，沒聽到吉克她的名字，但在一瞬間後她瞪圓了眼睛。

「果然不記得我了⋯⋯啊，對了！這個，這個這個！」

不知該怎麼辦，摸著腦袋的商人突然想起到了，把背著的書包放下來，開始整理起裡面的東西。就在她正覺得很不可思議，看著的時候，小販拿出幾本古書擋在了蕾蒂雪爾的面前。

「難道說，你是那時的小販？」
「是的。」

大概是被想起來感到很高興吧，小販用和藹可親的笑臉莞爾一笑。

「客人，您是貴族吧？前幾天失禮了。」
「沒事。」
「我也想在這樣的學校學習啊。這裡的圖書室很大，對於喜歡書的我來說，這兒不就是天堂嗎？」
「是啊，確實是天国。」

蕾蒂雪爾對面帶微笑的開朗小販有些不知所措。這種坦率而飄逸的人，是至今為止在蕾蒂雪爾周圍沒有過的類型，她在考慮普通情況下如何應對而猶豫不決。就在蕾蒂雪爾以微妙的表情沉默著時，小販的視線突然轉向了吉克。準確地說，是吉克拿著的書。

「那本書⋯⋯難道是『毛織物大全』嗎？」
「是的，您知道嗎？」
「那當然知道啦！我大概一年前讀過那本書，很有趣的。你喜歡紡織品嗎？」
「嗯。有時候也會自己編織。」
「手很靈巧啊。那也是你自己編的嗎？」

行商人指著掛在吉克腰上的毛織錢包說到。對著面帶親切笑容的小販，吉克也跟著微笑了起來。

「嗯，是的。」
「真了不起！我的這頂帽子也是毛織的手編哦。」

這樣說著的小販摘下戴著的毛織氈帽，露出了清爽的金色短髮。蕾蒂雪爾想到，雖然因為懶散的鬍子看起來很嚴厲，但是他和外表相反，是個看上去很年輕的人。

「那個獨特的編織物是⋯⋯難道是在利森那一帶買的嗎？」
「嗯，是這樣啊！判斷準確！」
「我的老家也在利森。」

丟下跟不上狀況的蕾蒂雪爾一個人，吉克和小販的對話漸漸熱烈了起來。蕾蒂雪爾重新認識到，所謂的共同話題蘊藏著無限的可能性。

「啊，順便說一下，我叫艾迪。對不起，自我介紹晚了。」
「謝謝您太客氣了，我是吉克。艾迪為什麼來這裡？」
「啊，我是來這個學校交付商品的。」

注意到他們兩人互相做自我介紹，快樂地說著話。這在蕾蒂希爾看來，他們的交流能力高得令人驚訝。

「啊，不好意思叫住了你們。吉克先生和多蘿瑟露小姐，非常感謝你們。」
「哪裡哪裡，彼此彼此。能和你說話很開心。」
「很榮幸能和你們談話。兩位貴人！」

互相打了招呼後，蕾蒂雪爾和吉克與艾迪分手了。

「真是一個開朗迷人的人啊。」
「嗯，是的啊。」

比起艾迪，蕾蒂雪爾更在意吉克為什麼對毛織物那麼了解。

「我完全不知道吉克的特技是編織物啊。」
「只不過是愛好而已。」

艾迪靜靜地目送著在庭院的對面消失的蕾蒂雪爾的背影。

一個人的腳步聲在沒有人的別館走廊中迴響著。艾迪在大圖書室門前停了下來。

「大衛先生，在嗎？」

推開圖書室的門，艾迪坦率地向櫃檯打招呼。被白鬍子掩埋的大衛磨蹭地站了起來。

「哎呀哎呀，這是⋯⋯艾迪閣下啊。」
「打擾了，大衛。腰還痛嗎？」
「呵呵，我還很精神著呢。」

為了證明這一點，大衛在椅子上咕嚕咕嚕地轉著。

「這次也拿到了珍貴的古書，我來交貨了。」
「一直以來不好意思了，這次是怎麼樣的書呢？」
「是這５本。」

艾迪把預定要交貨的古文書擺在了櫃檯上，然後大衛一本一本地拿在手裡仔細地翻閱著。

「說起來，這個學園好像正在進行改建工程，那是在做些什麼呢？」
「国王陛下要建立一個研究機關，是為了多蘿瑟露大人而建的。」

聽了大衛的話後，艾迪瞬間吃驚地瞪大了眼睛。

「⋯⋯多蘿瑟露大小姐⋯⋯是菲利亞利基斯公爵家的那個多蘿瑟露大小姐？」
「是的哦。」

艾迪回想起剛才在外面遇到的多蘿瑟露。

「多蘿瑟露大小姐在研究什麼？」

對於艾迪的提問，大衛沉默了一會兒，不久就小聲地說了出來。

「是魔術哦。」

聽到了自己問題的答案，艾迪什麼也沒說，就這樣用手捂著下巴沉思著。