在異世界的第２６天。
急忙趕往繪理家。今天也從早上開始治療。病情良好。今天就坐一天吧。

用治療和保鏢。我想聽聽借款和他們的事。Ａ級試驗不行也沒關係。好像是每４個月舉辦一次的祭典。

總之，早飯吃可攜式食物棒，麥片粥，水果，維生素、礦物質、鐵分和鈣的營養飲料，營養不良就消失了。

如果繼續好好地做了治療，暫且狀態異常消失了，不過，能否真的醫好是個疑問。

雖然不知道原來的病名，而且不知道是否能消除根本原因。哎呀，我倒覺得不會突然死掉的。

繪理非常高興，但她告訴了母親麗莎，讓她好好養病。

「畢竟，我是個會恢復魔法的商人，不是醫生。」
有點拘謹地說。希望大家好好聽我說。

「即便如此，還是感覺變好了。真的非常感謝。」
麗莎眼裡含著淚水。

「繪理。如果再不舒服的話，我還會在這附近。如果不在的話，請告訴王都的公會吧。」

「我一個人去不了王都。就算去了也不會放我進去的。」

「這樣啊。那麼，請這裡的公會轉達，讓他把留言傳到王都吧。和王都之間應該有頻繁的交流吧。」

「知道了。多謝。」
繪理也高興地說。

「那麼，昨天來的那些傢伙是什麼人？」

「那些人……是做地下生意的人，以高額利息借錢給沒錢的人。如果不能還錢，或不能支付利息的話，就會強行帶女人去店裡賣掉，或是拿走房子，為所欲為。國家的官員們裝做一副不知道的樣子。」

有罪。決定了。擊潰。

「那麼，借款金額呢？」
「１枚銀幣。最初是……。然後不斷增加，現在每月光是利息就５枚銀幣。與其借錢不如繼續窮……」

「最初借錢是在？」
「一年半前，爸爸去世後。」

繪理含著眼淚回答。
先把他們殺掉吧。啊，不行。我的想法完全靠近異世界。在日本根本沒考慮到這種程度。

不，是騙人的。一年到頭，都考慮著。我是不能原諒惡徒的體質。只是因為沒有力量，怎麼也做不到。

年紀不改性情。比起我，十幾歲的孩子死得更早。年紀沒用，對於不講理的事總是壓制不住憤怒的人。

要堅強啊，文明人。暫時，先弄壞「組織」，欠債擺爛嗎？商量一下吧。

我出錢也可以。但是那樣一定不行。未來必須靠自己的力量開拓。

「我去找人商量一下！」

「公會長～。」
我迅速轉移到了冒險者公會的公會長辦公室。哎呀，真方便啊，轉移魔法。

「……你，是怎麼來到這裡的。」
「轉移魔法。」
公會長抱著頭。

「不，我不會再說什麼了。行嗎？你絕對不要在人前使用那個—？」
「所以才偷偷地來到這裡不是嗎？」

有一位大叔，雙手扶著桌子，頭像要趴著似地抱著。不，我更是大叔。

「哎呀。在阿德洛斯的城鎮，有讓城裡的人哭泣的垃圾們，我想就算擊潰他們也沒問題吧。」

「你到底是什麼樣的頭腦構造，才會認為不會成為問題？那些傢伙和王都的貴族聯繫在一起。絕對不要出手。」

「這樣啊，貴族什麼的。在哪邊？主要是高官？還是下級呢？爵位說是到哪裡？」
因此第七感才警告著嗎？」

「啊，充其量也就是男爵或子爵吧。至於伯爵以上。考慮到從資產規模來看的好處和壞處，應該不會有那樣的事。至少，不是這個國家。王家不允許腐敗到那種程度。據說這是罕見的教導。
中央的文官貴族，因為沒有領地所以收入也很少，但也有喜歡與身份不相稱的奢侈生活的人。那樣的人們，注意到那樣的街去做壞事。國王也不應該那麼介意。因為很粗糙。」

「那麼，敗露怎麼樣？」
「啊，如果不至於成為問題的話，就無視吧？」
「那麼，對於問題【做了】的情況？」

「你……。話雖如此，不過如果以Ｂ級，不，Ａ級的冒險者與貴族頂撞的話，會怎麼樣呢？但是，你為什麼那麼想做？」
「那種事，肯定是稀人吧！」

沉默支配了相當長的時間。

「哎呀。怎麼了？」

「喂，喂，你。」
「誒？真討厭。就算隱瞞也暴露了喲？你啊，是這樣的吧？」

「不？不，不，怎麼會這樣想。」

「你天真了。看不到這個世界的人。你是轉生者嗎？明明是轉生者，為什麼不會覺得黑髮黑眼的我，很稀奇呢？」
啊，對了，他的眼睛是茶色的。頭髮也是很濃的焦茶啊。

「我繼承了稀人的血統。祖先是與王家有關連的人。」

「什麼呀，只是個子孫啊。是稀人的教導吧？這個子孫混蛋。老子是工作積極的現役稀人！所以遵循大和魂，決定討伐邪惡。」（譯註：バリバリ，大力做事，工作積極的人。）
沒有別的要做的事。大概，已經適應了這個世界。

「大和魂是什麼？」
「子孫的你早就忘了。」

公會長嘆了口氣，將下巴放在交叉的雙手上。

「原來如此。這發言簡直就像是在看１０００年前的傳說一樣。對了。稀人就是這樣的傢伙。我的祖先一定也是。」

阿蒙稍微思考了一下。
「要做的話，得等到Ａ級之後。那樣的話，你就去升到Ｓ級。如果到那種地步，就可以對和歹徒勾結在一起的雜魚貴族動手吧。你是救了王子的英雄。」

「誒—。」
那是什麼？

「嗯？我可以打嗎？」
「先成為Ｓ級給我看。話之後再說。」

「成為Ｓ級的條件是？原Ｓ級大人。」
「條件是取得國家認可的功績。戰爭英雄，屠龍者，或把什麼國家危機一下子救了出來，或向王家顯示顯而易見的功績。」

「你呢？」
「和龍對決後完成了。」
是那個啊。

「龍在哪裡？」
「我不知道野生的。應該在迷宮的深層吧。阿德洛斯應該也有。看看公會的資料。我也在隊伍中打過。去之前一定要來見我。因為要試驗。這是公會長的命令。」
「知道了。」

哦，不可以忘記。
「我有事想拜託你，我想保護我認識的一家人。想雇有本領的，不會被利誘，能絕對信任的人當護衛。別擔心，對方只是街上的流氓。不會說什麼要能和奇美拉對抗。」
「知道了。會準備的。」

「護衛一做好準備，就進入Ａ級試驗，進行各種各樣的修行。啊，護衛裡一定要有女人。還有龍的資料能拜託公會職員嗎？講解一下對龍的戰鬥法給我。」
「知道了。」

「拜託你了。屠龍者。」
「呼。多麼有問題的孩子。即使那樣也是稀人嗎！」
對不起。而且還是個年長的大叔。要說是哪個的話，是問題爺爺。

「啊—！忘記了重要的事情。」
「什、什麼。」

「對於那個護衛對象，說她有什麼事的話，要跟阿德洛斯的冒險者公會說。名字是繪理。希望它能傳達到你那裡。」

「知道了。這個也做吧。」
「請多關照～。」