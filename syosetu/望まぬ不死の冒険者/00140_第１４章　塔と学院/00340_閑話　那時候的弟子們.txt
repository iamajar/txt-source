「呣呣呣呣......」

都市馬路特一角的巨大宅邸，拉圖爾家的庭院。一邊發出這樣奇怪的呻吟聲，一邊努力集中注意力的，是雷特和羅蕾露的弟子，莉娜

附近還有同為二人弟子的艾莉婕，正認真觀察著她的動作

莉娜握緊了手中的初學者魔杖，向其中注入魔力

將注意力集中到極限，她猛然睜開眼睛，高喊

「......土箭！！」

短杖尖端空無一物的空間中，出現了一塊看起來像是土的棕色物體，慢慢變成了箭的形狀

當然，這個魔術不僅僅如此

隨著莉娜仿彿向前推出某種東西的動作，漂浮在空中的土箭，被以比普通箭矢稍慢的速度發射了出去

箭的前方有一個描繪著幾圈同心圓的標靶，土箭命中了最外側的一圈

看到這一幕的艾莉婕

「哦~！」

發出了歡呼，站在她旁邊的伊薩克也點了點頭

「以第一次嘗試縮短詠唱的結果而言，很不錯。嘛，要說不足之處的話，就是打得太偏了點......」

說出了嚴格的評價


沒錯


莉娜和艾莉婕兩個人，現在正在進行魔術特訓

被羅蕾露要求進行自主訓練的二人，同時也接到了這樣的指示：如果要練習攻擊魔術，必須在拉圖爾家管家，伊薩克的監督之下進行

因此二人才會在這裡進行練習

莉娜姑且不論，艾莉婕也被允許入內

本來，想要進入這裡，必須攻略那個令雷特都陷入苦戰的樹籬迷宮，但今天二人是通過捷徑進來的

當然，作為通關迷宮獎勵的魔道具就無法得到了，但二人對此並不知情，所以也沒關係了


倒不如說，身為孤兒院孩子的艾莉婕，來到這個滿是吸血鬼的空間，這件事本身就夠不妙的了。不過艾莉婕對此一無所知

她在向伊薩克以及家裡的其它傭人打招呼時，也完全沒有發現什麼不對勁的

這也是理所當然的，畢竟是在這個城市隱藏身份度過了幾十年，乃至上百年時光的吸血鬼們

就算來到據點的最深處，也沒辦法輕易看穿他們的真面目

不僅如此，艾莉婕還完全被種滿美麗薔薇的庭園、宏偉的宅邸、以及以伊薩克為首，著裝精美的僕人們所吸引，一瞬間就放下了心防

得知自己的孩子和吸血鬼這麼親密，母親們一定會大鬧一番吧，但能夠認識到這一點的人並不存在


唯一知道真相的人只有莉娜，但那樣的莉娜，本人也是吸血鬼的一員

艾莉婕如今已經不僅僅是籠中之鳥，簡直就是不死者砧板上的餌食。但既然此處並不存在想要捕食她的吸血鬼在，也沒有問題吧

「伊薩克先生，那個，我也辦得到嗎？」

艾莉婕向身旁的伊薩克問到。伊薩克雖然是吸血鬼，但還是馬上做出了無懈可擊的微笑，答道

「......遲早可以的。但是，想要馬上辦到就很難了」

「這是為什麼呢？」

「因為莉娜剛才並非普通的使出魔術，而是使用了縮短詠唱的技巧。說到詠唱魔術，可以分為普通詠唱、縮短詠唱、無詠唱幾種方式，難度也會依次遞增。艾莉婕小姐應該還沒學會普通詠唱呢吧？」

「......是。我，會不會沒有才能呢......？」

面對艾莉婕不安的問題，伊薩克微笑著搖了搖頭

「不，怎麼會呢。只是一開始就想要縮短詠唱的話，容易養成一些奇怪的習慣。怎麼說呢，魔術的詠唱，與劍術學習類似。掌握基礎之後，融入自己的風格進行改良、變化，和從一開始就自由揮舞劍，最終完成的劍術是不同的吧？前者可以算是某種流派的劍士，後者就應該稱為我流劍士吧。嘛，雖然哪邊才正確，哪邊更強還很難說......但我總覺得，前者的鍛鍊效率會更高一些呢？」

聽了伊薩克的解釋，艾莉婕點了點頭

然後，她想起了自己的一個老師，說到

「......說起來，雷特雖然很擅長使用魔力，但羅蕾露老師卻總說他使用魔術的方式很噁心」

「......呵呵，這我也有所耳聞。若以剛才的比喻來說，雷特先生就是所謂的我流劍士了吧，所以才會被那樣說。但是，雷特先生似乎也打算從新來過，而且他之前的努力也並非沒有作用，只要好好學習基礎，之後一定能夠起到作用的吧」

「確實，他也和我一起，跟著羅蕾露老師學習魔術」

「沒錯吧？莉娜已經學會了魔術基礎，因此才會進行下一個階段，也就是挑戰縮短詠唱。她比較不擅長土屬性的魔術，所以才需要努力練習。艾莉婕小姐也不必著急，首先還是應該把普通詠唱完全掌握。有我在一旁看著，你們不需要擔心失敗，好好練習吧」

「是！」

伊薩克雖然也能算得上是二人的魔術老師了，但他只是立在一旁，觀察著二人有沒有做出什麼危險舉動，適當的提出一些建議而已

在魔術基礎的學習方面，還是按照羅蕾露制定的教材進行，所以真正的老師果然還是羅蕾露吧


就這樣，二人的訓練還在繼續......