「什麼！？艾麗婭醬是帝國勇者團的一員……！？」

菲歐妮詢問艾麗婭為何和朱利烏斯皇子在一起行動後，對艾麗婭的回答感到驚訝不已。
因為憧憬劍聖艾莉莎，那個立志成為冒險者而踏上旅途的少女，竟然成為了維護一個國家和平安穩的軍團中的一員。


「啊哈哈，我自己也嚇了一跳……」

看著瞪大雙眼的菲歐妮，艾麗婭自己也苦笑了一聲。

「因為艾麗婭醬已經是A級冒險者了呢，而且還有和死靈龍戰鬥並擊敗其操縱者的實績在，會被帝國的勇者團招募進去亦是理所當然之事哦？」
「誒、誒……劍聖大人！？」


將手搭在苦笑著的艾麗婭肩膀上，艾莉莎也加入了對話中。
看到那個身姿，菲歐妮眼睛瞪得更圓，用更大的聲音喊道。


艾莉莎是拯救了魯米納斯的英傑，她在這個村裡是無人不知、無人不曉的。
剛才菲歐妮的注意力一直在艾麗婭和朱利烏斯皇子的身上，完全沒有發現在他們身後的艾莉莎。

「艾麗婭醬，這是怎麼回事！？為什麼劍聖大人會在這裡……而且還那麼親近……！」
「菲歐妮醬，是吧？帝國那邊有委託，所以我也要參與到這次戰役裡去。而為何我和艾麗婭醬這麼親近嘛，因為她是像我的弟子一般的存在吧」
「哈……！？」


艾莉莎代替艾麗婭回答了菲歐妮的質問，令菲歐妮又一次發出了驚訝的聲音……然後當場驚呆了。
艾麗婭才踏上旅途沒多久，期間到底發生了什麼……想必她腦海裏一直在探究這個問題吧。


「菲歐妮姐姐，我們先進村吧，要給村長傳達殿下已經到達了的消息才行啊」
「啊，說得對，艾麗婭醬……我先帶大家到村長所在之處，請隨我來」

在艾麗婭的提醒下，菲歐妮總算是取回了冷靜，招呼大家進到村子裡去。


「嗚哇~！裡面綠油油的呢！」
「果然有樹木在就安心多了~！」

一進到村裡，莉莉和菲莉就歡騰了。


金屬的堅壁內側，是綠意滿盈的世界。
建築物也是以木造的居多，除去那個被金屬覆蓋的外側建築的話，這村落才是那個跟世人印象中一致的，與自然融為一體的精靈們的住處。

「村長的住所在村子的正中央」

菲歐妮一邊說著一邊往前走。
她身旁的雷歐也跟著一起走……不過莉莉和菲莉還在它的背上「哇~吚，毛茸茸~！」又飛又坐地嬉戲不止。


當事貓的雷歐則一臉「哼，這兩隻小玩意真讓貓沒辦法唉」的表情容許了二人的玩鬧。


「比起幾年前變化還真是大呢，不過總感覺很懷念……」


環視著周圍的艾麗婭輕輕地感嘆，像是回憶起當年魔族大軍侵略這裡時她被救下來的那個光景。


「這裡就是艾麗婭的故鄉嗎，真是個美妙的地方呢♪」
「呼呼，謝謝你，安娜桑」


故鄉被阿納爾德稱讚使艾麗婭感到十分高興，她臉上不由得露出了笑容。


「來自帝都的朱利烏斯殿下和他的從者們已蒞臨於此」


走了不一會兒，菲歐妮來到一間木造的大屋子前，敲了敲門。


接著很快便有像是傭人一般的女精靈打開了門，接著她向眾人優雅地躬身一禮，招待大家進到裡面去。


「歡迎諸君的到來，朱利烏斯殿下、各位閣下……！我是這個村落的村長「埃魯文」，初次見面還請各位多多關照」


走進這個廣闊的房子後，一位男性的精靈走了過來迎接眾人。
那是精靈族特有的美型男，雖說村長之類的人物聽起來就像是上了年紀的那類人……不過精靈族的外貌一生都顯得很年輕所以這也是當然的事了。


村長——埃魯文在看到與朱利烏斯皇子一同進來的艾麗婭後感到十分驚訝，但他也明白比起她的事現在還有更為重要的事項需要商討。


他帶領大家來到房子中央的長桌後，便直入主題。
莉莉和菲莉，還有其他冒險者們對他們要討論的東西一竅不通……乾脆就在一旁的沙發上放鬆下來稍作休息。


「那麼，埃魯文村長，我想相關的書信你已經過目了，那麼我們就先對村裡的既有戰鬥部隊和戰力進行統計吧」
「當然沒問題，朱利烏斯殿下。自從幾年前我們的村落被魔族襲擊之後，我們不僅修建了村外的城牆，還進行了戰力的強化。如今直接和魔族開戰也不存在問題」
「嗯，此事略有耳聞。你們應該不僅僅只強化了內部的戰力，還聯合其他村落，擴展了戰力吧？」
「正是如此，殿下。特別是這一年新組成的「亞馬遜部隊」，擁有非常強大的戰鬥力之餘還兼具極高的靈活性，所以我想戰力的合併應該也會很順利的」


朱利烏斯皇子和埃魯文村長開始對戰力的合併方法、周圍地形的掌控、防禦的模式等等進行討論研究，不斷將話題推進下去。


(呼呣，看起來魯米納斯的戰力確實要比以前強化了不少吶)


被艾麗婭抱著的塔瑪一邊看著他們討論一邊心想。


埃魯文村長身旁還站著兩名作為參謀的男性精靈，在埃魯文村長回答朱利烏斯皇子的疑問時，也從旁適當地補充一些必要的情報。


想要防守據點只是單純增強戰力是沒有意義的，要調度那樣的戰力也需要鍛鍊相應的智慧。能理解到這一點的精靈族們，比起多年前那次襲擊發生時明顯要成長了不少。


塔瑪對這個事實在心裡由衷地讚嘆。


(戰爭之事，不發生才是最好的。不過，這個村子裡存有的戰力到底強到什麼程度很讓貓在意啊)


以強大的存在為目標的騎士，對其他人的強度在什麼水平之類的事難免會躍躍欲試。


可能察覺到在自己懷中興奮難耐的塔瑪的異樣，艾麗婭將其埋到更深的地方緊抱著，像寵溺嬰兒般滿懷愛意地撫摸著他的小腦袋。


艾麗婭的體香、柔軟的觸感和那份溫柔，讓塔瑪不禁發出了「呼喵~……」的一道可愛的叫聲。
