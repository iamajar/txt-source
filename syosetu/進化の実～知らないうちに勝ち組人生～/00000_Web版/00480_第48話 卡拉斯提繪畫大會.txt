幫助藍澤先生，請露易絲她們指導俺的技能和魔法的訓練，到現在已經過了1個月了。
在這1個月裡，俺有時會與莎莉婭以及阿爾一起去購物，偶爾接受採集類的委託還有討伐史萊姆的簡單委託等等，過著這樣的生活。
王城裡所發生的事並沒有跟莎莉婭和阿爾提及，因為這是約定嘛。
但是，作為王都杯的獎勵變成了幫俺進行技能和魔法方面的訓練的這件事已經向她們傳達過了。
可是在當時，不知為何阿爾露出的可怕的表情，而莎莉婭也一臉複雜的表情。
到底是怎麼了嘛？
露露妮在那之後並沒有變回驢馬，而是經常跟俺一起去路邊小食攤那吃吃喝喝。
......雖然並沒有擔心錢的問題，但露露妮的食欲真的很不妙。
輕而易舉就吃下了俺食量數倍以上的食物......
不管怎麼樣，總之俺到現在為止都一直過著充實的生活，而今天因為某件事情要外出。
這件事情便是關於卡拉斯提繪畫大會的。
在這個大會上，克雷以及梅茵兩個人都會出席，因此俺是不去不行。
跟莎莉婭，阿爾還有就是露露妮她們說過後，三個人都表示要跟著來，所以俺們就一起前往會場那裡，而現在正在去往的途中。

「畫啊......那是咱不大瞭解的世界呢......。」

阿爾雙手十指交叉放在腦後，發起了這樣的牢騷。

「唔......我也不太明白這種事呢。誠一你清楚嗎？」

對阿爾的話有所反應的莎莉婭向俺問道，因此俺這邊也老實回答了。

「不，老實說俺也不太懂這方面的事。露露妮應該也不懂的吧？」
「誒？我知道的喲？」
「哈！？」

聽到露露妮那個不假思索的回應，俺不禁發了聲音。
明明只是頭驢......
竟然能理解藝術......！？
接著，露露妮挺起胸脯自信地答道。

「藝術......即是進食（吃吃吃）吶。」
「很好，你完全不懂得哪。」

因為露露妮一如往常，俺安心了。
在這之後，我們一邊聊著些無關緊要的話一邊走著，終於都到達了大會的會場。

「就是在這裡舉辦的嗎......？」

卡拉斯提繪畫大會的舉辦會場就是俺第一次遇到梅茵的那個廣場。
雖然之前總是有許許多多的小食攤位在這裡營業著，但今天一個路邊攤也沒有，取而代之的是個巨大的舞臺，而在上面擠滿了許多的人。

「好膩害啊......雖然這裡的人應該並不全都是參加者，但在畫家應該就是這些人當中哪一位吧。」
「吶，誠一，不去和那個叫梅茵的人會合好嗎？」

正當俺對會場裡聚集著的人數之多感到驚訝的時候，阿爾跟俺這樣問道。

「沒關係的喲。而且，在這樣的茫茫人海當中找出來也很困難的吧？」
「確實啊......。」

阿爾也對這人滿為患的情況露出了苦笑。
但是......
真的是好多人啊。
雖然因此也有許多超級奇怪的人們集聚在這裡。
例如，那個裝扮成小丑一樣的人。
但在這麼濃密的人口密度之中，到底還是無法進行表演的吧。
其他的還有，穿著那種設計無法形容的服飾的人，以及拿著奇怪雕像走著的人等等，很多在這個街鎮沒看到過的奇怪的人群都已經集結在這裡了。

「真不愧是藝術家......徹底貫徹著自我表現欲......。」

暗地裡對周圍的人們感到戰慄的時候，突然聽到了跟王都杯那時一樣的廣播聲。

『──讓各位久等了！現在就開始舉行卡拉斯提繪畫大會！』
「「「嗚噢噢噢噢噢噢噢噢噢噢噢噢噢！！」」」

聽到年輕男子的宣佈之後，會場裡的人們的熱情立即高漲起來了。

「哇啊！氣氛好熱烈啊！」
「就是說啊......俺也沒想到竟然會是氣氛這麼高漲的大會啊......。」

聽到莎莉婭欽佩的聲音，俺也表示同意。
而阿爾和露露妮兩人都一樣對周圍發出的歡呼聲感到不知所措。

『那麼，接下來就讓我來介紹一下這次大會的評委吧。擔任此次大會評委一職的人便是，確立了許多種畫法的繪畫天才──雷恩‧貝爾加大人。」

聽到那樣的廣播聲之後，俺聽到旁人所發出的驚訝的聲音。

「騙，騙人的吧！？是那個『畫聖』雷恩嗎！？」
「不僅推廣了現如今聞名的抽象畫，而且也是遠近法和陰影畫法的開拓者啊......。」
「評論都說那個人的存在讓現代繪畫進步了100年喔......。」

聽到他們低聲細語的內容，俺震驚了。
即使是對地球的畫家來說理所當然的技法，在這個世界也肯定是不一樣，而如果是在這個世界創造出了連俺也知道的那種技法的話，那還真是相當厲害的人啊。
話說，貝爾加這個家名，總感覺在好像在哪裡聽過......
當俺想不起在哪裡聽過這家名的時候，與王都杯一樣的形式，一位老人家的身影通過魔力投影機投映在空中了。
露出柔和的笑容的這位老人家應該就是名為雷恩先生的人了吧。
雖然現在年老了，但看他現在的容貌就可以知道他年輕的時候也是一名英俊的人。
然後，雷恩先生以和藹的聲音說道。

『愉快地繪製出注入了你們情熱的最高作品吧。』
「「「嗚噢噢噢噢噢噢噢噢噢噢噢噢噢噢噢噢噢噢噢！！」」」

比剛才還要熱烈的氣氛包圍了整個會場。
好厲害啊！
這位老人家對畫家來說應該是極為憧憬的存在吧。

『雷恩大人，非常感謝您的發言。那麼，我想立即去到審查的環節。有請1號參賽者。』

就這樣，卡拉斯提繪畫大會順利地開幕了。
不過，還有一點挺遺憾的，因為人太過多了，所以無法靠近舞臺那邊，想好好鑒賞一下畫作都做不到，只能通過投映在上空的畫面來進行確認。
這種情況只能認為是沒有辦法的事。
俺們眺望著上空的影像，總算是看到了各種各樣的畫作陸續登場。
那些畫作，既有會讓人誤以為是實物那樣的相當逼真的風景畫，也有像克雷那種不是太明白畫了什麼東西的畫，真的是各式各樣的畫都有。
然後，雷恩先生對那裡的作品一件一件地進行叮囑般的評價，好的地方以及不好的地方兩方面都評價了。
並不只是指出差的地方而且連同好的地方也被指摘出來了，因此，被評價的參賽者全員都感激涕零的樣子。

『──好了，現在還剩餘最後的兩位參賽者了。那麼，有請下一位參賽者。』

然後，主持人如此宣告之後，登上舞臺的是，邁著正氣凜然的步伐的克雷。
......啊，沒錯啊！
還在想是在哪裡聽過這家名，這不就是與克雷的家名一樣的嗎！？
事到如今才注意到這種事而驚訝著的時候，聽到了從周圍傳來了的聲音。

「他是『畫聖』的孫子......。」
「浩氣凜然的身姿貨真價實啊！」
「嗯......會拿出怎樣的作品呢......？」

大家都在關注著克雷的作品。
雖然俺是不太理解克雷的畫作，但對於周圍的藝術家們而言，應該就不一樣了吧。
一邊考慮著這種事情，一邊眺望投映在上空的克雷的身姿。
跟著登場的是一個長寬有克雷身長一倍左右大小的，被布料覆蓋著的作品。

「好，好大啊對吧？其他的畫顯得更加小了呢？」

看到這太過於巨大的畫，身旁的阿爾這樣向俺問道。
......雖然認識的時間很短，但俺還是知道克雷他對自已有著很充足的自信，就算是這樣俺也是受驚過度。

『克雷‧貝爾加先生，請您發表作品名。』

被主持人如此催促到，克雷氣勢凜然地發言，同時也揭開了蓋在作品上面的布料。

『我的作品名......就是『藝術』！』

出現在布料下面的是，繪畫在純白的畫板上的赤紅太陽。
沒錯，就是『太陽』。

「那，那個克雷的畫......竟然會這麼正經......！？」
『......到底是為什麼呢？剛才，有種好像我被當成大笨蛋那樣的感覺......』

無視出現在影像中的露出複雜表情的克雷所嘀咕著的話，俺打從心底感到了震驚。
因為這可是那個把只是個三角形的畫說成是『在夕照下的海濱凝望著夕陽，思念著戀人的少女的畫』的傢夥的畫喔？
現在眼前這幅正被投映出來的畫作所畫的是，燃燒得通紅的巨大太陽。

「誠一，你的朋友不是畫了幅超厲害的畫嗎！？」
「嗯嗯！是個很豔麗的太陽啊！」

阿爾和莎莉婭都對克雷的畫作感到驚訝。

「嗯......我個人覺得，這看起來好像蘋果啊......。」

露露妮小姐，那是只有你才看得到的。
話雖如此，在那純白的畫板上描繪著的太陽......
這簡直就像是日本的國旗啊。
儘管突然間有這樣的想法，但聽得懂的人，在這個地方應該是沒有的吧。

『克雷先生，那麼就請您介紹一下作品。』
『那好吧！首先，這個作品所注入的，是我對藝術的感情！熊熊燃燒的這種感情......不正是與火柴棒的火焰一樣嗎？』

這感情是有多渺小啊......！
話說，居然不是太陽！？
那種尺寸的火柴棒火焰！？
看起來就是太陽啊！？
明明都細心到把太陽的耀斑都畫出來了啊！

『因此，我的畫作與其他的各位是不一樣的，除此之外的多餘的事物都沒有畫上去。這個純白的畫板就是我的身體，而正中間的火柴的火焰，就是我對藝術的熱情啊！』
『原，原來如此......但在我看來，這個看起來好像跟太陽一樣......？』
『這是火柴的火焰！別給我把這個搞錯了。』
『哈，哈啊......』

克雷喲，你果然就是你，真是一點都沒變啊......
明明還以為是正經的畫作，到頭來絲毫沒有一點改變，對於這樣的克雷，俺在某種意義上感到安心了。
接著，這次是輪到雷恩先生來進行評審。
雷恩先生稍微沉默了一小段時間，不久後便悠然地開口說道。

『──我也感覺這看起來像是火柴棒的火焰啊。』

你也一樣嗎！？
俺在心裡吐槽起雷恩先生的發言。
明明至今為止都作出了正經的評價，但到了這一塊，卻展示出與克雷流著同一種血的一面，這樣好嗎！？
正當俺浮現出愕然表情的時候，再次聽到了從周圍傳來的聲音。

「......真是晦澀啊！」
「啊......這畫艱深晦澀啊！」
「真不愧是，『畫聖』的孫子啊......。」
「把那個斷言為火柴的火焰，這是......。」

為什麼你們會如此戰慄啊！？
身為一般人的俺很奇怪嗎？
藝術家好恐怖啊。
正當俺一個人擅自對藝術家感到膽怯的時候，雷恩先生以和藹的聲音說道。

『克雷......你從以前開始，就是一個坦率地畫畫的孩子呢。確實，雖然這對旁人來說，也許只是幅難以理解的的作品，但即使是這樣，在你所描繪的畫作中也被傾注了某一種感情。而這次的作品，看起來像是太陽，然而你卻一口咬定說這並不是太陽而是火柴棒的火焰，這應該是指，表現了火柴最終燃燒殆盡的那一瞬間吧？』

『真不愧，是爺爺大人......我的人生，並沒有像太陽那麼宏大。但是，在我這個渺小的人生當中，也希望能宛如太陽那樣激烈的燃燒起來......這就是，我的藝術！』

好深奧......！
超深奧的啊，雷恩先生！
不，這裡應該是說，畫出這種畫的克雷好厲害啊。
聽了雷歐先生的說明，總感覺俺好像理解了克雷所表現的東西了。
......果然，這是凡人之軀的俺無法理解的領域啊。
那也就是說，那個不知道還可不可以稱之為三角形的畫，還真是描繪了『在夕照下的海濱凝望著夕陽，思念戀人的少女』的嗎？
......糟糕，還是完全不懂。

『呼呼。克雷......你的熱情，已經很好地傳達給我了。因此，你的那種感情，不僅是我，而且也要努力傳達給更多的人知道......這就是你的課題啊。』
『！明白！』

就這樣，克雷的作品的評審結束了。
也就是說，接下來作為最後一人出場的──

『......那麼，終於都到是最後一位參賽者登場了。有請，梅茵‧切裡小姐上臺』

──就是梅茵。
這麼宣佈之後，看起來一臉緊張的梅茵登上了舞臺......梅茵，妳走起路來同手同腳了喲......
緊跟在這樣的梅茵後面登場的是，與克雷那個同等尺寸大小的，被布料覆蓋著的作品。
看到緊張得直打顫的梅茵，身旁的阿爾擔心地說道。

「喂，喂......沒問題嗎？那個孩子，看到這邊都擔心得緊張起來了不是嗎？」
「啊......嗯，是這樣子啦。但是，俺們就算想做點什麼也做不到啊......。」

沒錯，俺們束手無策啊。
正因為如此，梅茵必須得自已好好加油努力啊。
俺在注視著梅茵的同時也在心中為她應援著。

『那麼，有請梅茵小姐介紹一下作品名。』
『好，好滴！？』

又緊張了啊。
被主持人搭腔的梅茵，犬尾觸電般地伸直了，而且還硬直了。
但是，梅茵靠自已的力量解除了硬直，慢慢地進行深呼吸，讓心情鎮靜下來了。
然後──

『這，這就是......我的作品！』

梅茵揭開畫布的瞬間，出現在那裡的畫面是──

「誒！？」

描繪了擺著與憑藉英雄拿破崙的肖像畫而聞名的，雅克‧路易‧大衛所創作的『跨越阿爾卑斯山聖伯納隘道的波拿巴（拿破崙）』一樣pose的，騎著驢馬形態的露露妮的俺的身姿......！
